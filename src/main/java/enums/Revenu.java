package enums;

public enum Revenu {

	SALAIRE("Salaire"),
	VIREMENT("Virement"),
	TRAITEMENT("Traitement"),
	LOYER("Loyer"),
	VENTE("Vente"),
	AIDE("Aide");
	
	private String categorie = "";
	
	Revenu(String categorie) {
		this.categorie = categorie;
	}
	
	public String toString()	{
		return this.categorie;
	}
}
