package enums;

public enum TypeDeCompte {
	
	COMPTE_VISA("Compte Visa"),
	LIVRET_A("Livret A"),
	LIVRET_B("Livret B"),
	PLAN_EPARGNE_LOGEMENT("Plan Epargne Logement"),
	LIVRET_JEUNE("Livret Jeune"),
	LIVRET_EPARGNE_POPULAIRE("Livret Epargne Populaire"),
	LIVRET_DE_DEVELOPPEMENT_DURABLE("Livret De Développement Durable"),
	COMPTE_BANCAIRE("Compte Bancaire");
	
	
	private String intituleDuTypeDeCompte = "";
	
	TypeDeCompte(String intituleDuTypeDeCompte)	{
		this.intituleDuTypeDeCompte = intituleDuTypeDeCompte;
	}
	
	public String toString() {
		return this.intituleDuTypeDeCompte;
	}

}
