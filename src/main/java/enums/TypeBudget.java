package enums;

public enum TypeBudget
{
    MENSUEL("Mensuel"),
    SEMESTRIEL("Semestriel"),
    TRIMESTRIEL("Trimestriel");

    private String libelle = "";

    TypeBudget(String libelle)
    {
        this.libelle = libelle;
    }

    public String toString()
    {
        return this.libelle;
    }

}