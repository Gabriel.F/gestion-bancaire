package enums;

public enum Depense {
	
	LOYER("Loyer"),
	VIREMENT("Virement"),
	MENSUALITE("Mensualité"),
	ACHAT("Achat"),
	FACTURE("Facture");
	
	
	private String categorie = "";
	
	Depense(String categorie) {
		this.categorie = categorie;
	}
	 
	public String toString() {
		return this.categorie;
	}

}
