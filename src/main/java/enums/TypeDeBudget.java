package enums;

public enum TypeDeBudget {
	
	MENSUEL("Mensuel"),
	SEMESTRIEL("Semestriel"),
	TRIMESTRIEL("Trimestriel");
	
	
	private String libelle = "";
	
	TypeDeBudget(String libelle) {
		this.libelle =libelle;
	}
	
	public String toString() {
		return this.libelle;
	}
}
