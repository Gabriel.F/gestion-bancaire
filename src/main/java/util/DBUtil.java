package util;

import com.esotericsoftware.yamlbeans.YamlReader;

import java.io.FileReader;
import java.net.URLDecoder;
import java.util.Map;

/**
 * Util class providing the different properties of a YML configuration file
 * Créé par Gabriel le 11/01/2017.
 */
public class DBUtil
{
    public static Map getCredentials(String fileName)
    {
        Map map = null;
        try
        {
            YamlReader ymlReader = new YamlReader(
                    new FileReader(URLDecoder.decode(
                            DBUtil.class.getClassLoader().getResource(String.format("config/%s.yml", fileName)).getPath())));
            map = (Map)ymlReader.read();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return map;
    }
}