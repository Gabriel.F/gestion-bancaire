package dao.sql;

import dao.DAO;
import org.jooq.DSLContext;
import org.jooq.impl.DSL;
import util.DBUtil;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Map;
import java.util.Properties;

/**
 * Parent DAOImpl class providing basic database access and CRUD
 * Every other more specific DAOImpl class will inherit this one
 * Créé par Gabriel le 11/01/2017.
 */
public class DAOImpl<T, PK> implements DAO
{
    protected DSLContext jooqConnection;
    protected EntityManager entityManager;

    private Class<T> tClass;

    public DAOImpl(String ymlFilename, Class<T> tClass)
    {
        Map dbCredentials = DBUtil.getCredentials(ymlFilename);
        String connectionString = String.format("jdbc:mysql://%s:3306/%s?serverTimezone=UTC",
                dbCredentials.get("address"),
                dbCredentials.get("db"));

        /*
         Initializing EntityManager by overriding
          */
        Properties customProperties = new Properties();
        customProperties.setProperty("javax.persistence.jdbc.url", connectionString);
        customProperties.setProperty("javax.persistence.jdbc.user", dbCredentials.get("user").toString());
        customProperties.setProperty("javax.persistence.jdbc.password", dbCredentials.get("password").toString());
        entityManager = Persistence.createEntityManagerFactory("MyBank", customProperties)
            .createEntityManager();

        /*
         Intializing JOOQ
         For every basic request, we will use the CRUD operations provided by the DAO abstraction
         JOOQ will be used for every "complex" request for a readability concern
          */

        try {
            Connection conn = DriverManager.getConnection(connectionString,
                dbCredentials.get("user").toString(),
                dbCredentials.get("password").toString());
            jooqConnection = DSL.using(conn);
        } catch(SQLException e) {
            e.printStackTrace();
        }

        this.tClass= tClass;
    }

    @Override
    public void create(Object newObject)
    {
        entityManager.persist(newObject);
    }

    @Override
    public Object read(Object id)
    {
        return entityManager.find(tClass, id);
    }

    @Override
    public void update(Object transientObject)
    {
        entityManager.merge(transientObject);
    }

    @Override
    public void delete(Object persistentObject)
    {
        entityManager.remove(persistentObject);
    }
}
