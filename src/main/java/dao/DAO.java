package dao;

/**
 * An interface providing basic CRUD operations for DAO classes
 * Créé par Gabriel le 15/07/2017.
 */
public interface DAO<T, PK>
{
    /** Persist an object into database */
    void create(T newObject);

    /** Retrieve an object from the database */
    T read(PK id);

    /** Save changes made to a persistent object.  */
    void update(T transientObject);

    /** Remove an object from the database */
    void delete(T persistentObject);
}
