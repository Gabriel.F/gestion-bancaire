package dao;

import dao.sql.DAOImpl;
import entities.Budget;
import entities.Utilisateur;

/**
 * A factory providing access to all DAO instantations, thus centralizing everything
 * Personal note : singleton could be nice to avoid multiple instantations of the same DAO object, but
 * I don't know if this a good practice
 * Créé par Gabriel le 13/01/2017.
 */
@SuppressWarnings("unchecked")
public class DAOFactory
{
    public static DAO<Budget, Integer> getBudgetDAO(String ymlFile)
    {
        return new DAOImpl<Budget, Integer>(ymlFile, Budget.class);
    }

    public static DAO<Utilisateur, Integer> getUtilisateurDAO(String ymlFile)
    {
        return new DAOImpl<Utilisateur, Integer>(ymlFile, Utilisateur.class);
    }
}
