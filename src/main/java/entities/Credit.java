package entities;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

/**
 * "Credit" JPA entity
 * Créé par Gabriel le 02/07/2017.
 */
@Entity
@Data
@NoArgsConstructor
public class Credit
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Basic
    @Column(length = 11)
    private int montant;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "date_operation")
    private Date dateOperation;

    @Basic
    @Column(length = 128)
    private String libelle;

    @OneToOne
    @JoinColumn(name = "FK_Credit_id")
    private Operation operation;
}
