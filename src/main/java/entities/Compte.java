package entities;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

/**
 * "Compte" JPA entity
 * Créé par Gabriel le 02/07/2017.
 */
@Entity
@Data
@NoArgsConstructor
public class Compte
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Basic
    @Column(length = 32)
    private String intitule;

    @Basic
    @Column(length = 11, name = "taux_crediteur")
    private int tauxCrediteur;

    @Basic
    @Column(length = 64)
    private String type;

    @Basic
    @Column(length = 11)
    private int solde;

    @Basic
    @Column(length = 34)
    private String iban;

    @OneToMany(mappedBy = "compte", cascade = { CascadeType.PERSIST, CascadeType.MERGE })
    private List<Operation> operations;

    @OneToMany(mappedBy = "compte", cascade = { CascadeType.PERSIST, CascadeType.MERGE })
    private List<Budget> budgets;

    @OneToOne
    @JoinColumn(name = "FK_Compte_id_Utilisateur")
    private Utilisateur utilisateur;

    @OneToOne
    @JoinColumn(name = "FK_Compteè_id_Banque")
    private Banque banque;
}
