package entities;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * "Utilisateur" JPA entity
 * Créé par Gabriel le 02/07/2017.
 */
@Entity
@Data
@NoArgsConstructor
public class Utilisateur
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Basic
    @Column(length = 32)
    private String login;

    @Basic
    @Column(length = 128)
    private String password;

    @Basic
    @Column(length = 32)
    private String nom;

    @Basic
    @Column(length = 32)
    private String prenom;

    @Basic
    @Column(length = 64)
    private String email;
}
