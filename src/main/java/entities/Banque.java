package entities;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * "Banque" JPA entity
 * Créé par Gabriel le 02/07/2017.
 */
@Entity
@Data
@NoArgsConstructor
public class Banque
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Basic
    @Column(length = 64)
    private String nom;

    @Basic
    @Column(length = 11)
    private String bic;
}
