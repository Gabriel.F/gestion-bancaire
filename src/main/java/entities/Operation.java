package entities;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

/**
 * "Operation" JPA entity
 * Créé par Gabriel le 02/07/2017.
 */
@Entity
@Data
@NoArgsConstructor
public class Operation
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Basic
    @Column(length = 11)
    private int montant;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "date_operation")
    private Date dateOperation;

    @Basic
    @Column(length = 128)
    private String libelle;

    @ManyToOne
    @JoinColumn(name = "id_Compte")
    private Categorie categorie;

    @ManyToOne
    @JoinColumn(name = "id_Categorie")
    private Compte compte;
}
