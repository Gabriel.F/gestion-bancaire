package entities;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

/**
 * "Emprunt" JPA entity
 * Créé par Gabriel le 02/07/2017.
 */
@Entity
@Data
@NoArgsConstructor
public class Emprunt
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Basic
    @Column(length = 64)
    private String intitule;

    @Basic
    @Column(name = "montant_total")
    private float montantTotal;

    @Basic
    @Column(name = "taux_interet")
    private float tauxInteret;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "date_depart")
    private Date dateDepart;

    @OneToOne
    @JoinColumn(name = "FK_Emprunt_id_Compte")
    private Compte compte;
}
