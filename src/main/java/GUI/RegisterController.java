package GUI;

import lombok.extern.slf4j.Slf4j;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

/**
 * Controller handling new users registration
 * Créé par Gabriel le 16/06/2017.
 */
public class RegisterController
{
    public void handleRegistration(String password)
    {
        try
        {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(password.getBytes(StandardCharsets.UTF_8));
            String encoded = Base64.getEncoder().encodeToString(hash);
        }
        catch(NoSuchAlgorithmException e)
        {
            // Do something
        }
    }
}
