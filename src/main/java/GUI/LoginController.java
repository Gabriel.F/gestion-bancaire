package GUI;

import dao.DAO;
import dao.DAOFactory;
import entities.Utilisateur;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import org.controlsfx.control.Notifications;

/**
 * Controller handling user logging
 * Créé par Benjamin
 */
public class LoginController {

    @FXML
    private TextField username;
    @FXML
    private PasswordField password;

    @FXML
    public void handleAcceptLoginAction(ActionEvent actionEvent)
    {
        DAO<Utilisateur, Integer> utilisateurDAO = DAOFactory.getUtilisateurDAO("local");
        // Just an example showing DB access is working
        Utilisateur utilisateur = utilisateurDAO.read(1);
        // Notifies something, here that the user submitted his credentials
        Notifications.create().title("Information")
                .text(String.format("Login requested for : \nUser %s\nPassword %s", username.getText(), password.getText()))
                .position(Pos.BOTTOM_RIGHT)
                .show();
    }
}
